---
title: "New maintenance release with UTF-8 support: v1.2.1 "
date: 2020-09-09T11:24:05+02:00
tags: ["release"]
draft: false
---

This version introduces some changes and bug fix you can see below. It brings you UTF-8 support to allow extended characters support.

### Release note


#### Changes:
* Ouptut string encoding set to UTF-8 --> [#27](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/27)

* If needed for recording the output in a file, folder arborescence is created --> [#28](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/28)

#### Bug Fix :

* Rendering issue when paragraph is empty --> [#14](https://gitlab.com/wovalab/open-source/asciidoc-toolkit/-/issues/14)
<!--more-->

### Package Download

Package is directly available through [VI Package Manager](https://www.vipm.io/package/wovalab_lib_asciidoc_for_labview/).

### LabVIEW supported version

2014 (32 and 64 bit) and above

